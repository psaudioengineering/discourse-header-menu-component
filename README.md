# PS Audio Discourse Header Menu

## Overview

This is PS Audio's current header menu on our Discourse Forums.

## Getting Started

1. Download, configure, and start a local development environment of [Discourse](https://github.com/discourse/discourse). I prefer the [Docker setup](https://meta.discourse.org/t/install-discourse-for-development-using-docker/102009).
> Note: you may need to manually install Node dependencies by running `d/exec yarn install` *before* running the `d/boot_dev --init` command. See https://meta.discourse.org/t/install-discourse-for-development-using-docker/102009/259?u=kevinbriggs.
2. `git clone git@bitbucket.org:psaudioengineering/discourse-header-menu.git` --- clones this repository
3. Navigate to the 'Customize' tab of your [local Discourse dev admin console](http://localhost:4200/admin/customize/themes)
4. Select the 'Components' option from the left-side menu
5. If this is your first time working with this Header component, hit the 'Install' button at the bottom of the Components menu.
6. Select 'Create new' and enter a name and set the Type as Component then hit 'Create' to make a new empty component.
7. Hit the button for 'Edit CSS/HTML'
8. Copy and paste the files from this repository into the corresponding section in your local Discourse
  - For instance, copy all of common.scss into the 'CSS' tab in your Discourse Dev environment.
  - Do the same for head_tag.html into `</head>`, after_header.html into 'After Header', and body_tag.html into `</body>`
9. Make any changes directly through the 'Edit CSS/HTML page in your local Discourse environment, saving afterwards to view the changes.

## Deploying Changes

Once you have things how you would like them, copy the updated HTML and CSS files over from your local Discourse environment into their corresponding files here. Commit these changes to the 'Main' branch of this repository.

Next, go to the ['Customize' dashboard of our production forum](https://forum.psaudio.com/admin/customize/themes). Select the Header Menu Component and hit the 'Check for Updates' button. You should see a notice that you are x commits behind with the option to update. If you're confident with your changes, select update.

## Some notes on development and deployment

The process outlined above is suboptimal in some ways, though it is in place for two reasons:

  1. This allows for easy previewing of edits locally.
    - It would be possible to instead configure your local dev environment to also pull from this git repository (even using a separate development branch if desired); however, to my knowledge the only way to propogate changes to your local environment in that case would be to commit and push any changes.
  2. The process as outlined above also minimizes the risk of development work accidentally getting pushed to our production forum.

This approach is not ideal for some obvious reasons, though given the infrequent updates we make to the forum and its components, this method allows for the simplest development process with the least risk of things going haywire.

## Contributors

- Scott Schroeder (scott.schroeder@psaudio.com)
- Kevin Briggs (kevinb@psaudio.com)